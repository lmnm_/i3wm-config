# i3wm-config

This is my i3-WM config. It is a mix of the base settings of EndeavourOS themed to my taste.  
I assume that this repository's root is your home folder.  

## Heavily inspired by the Monokai Pro color scheme

- Programs
  - Editor : VSCode with Monokai Pro theme.
  - Shell : zsh (manually installed plugins : [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions) and [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)).
  - Prompt : [Starship](https://starship.rs/).
  - Terminal emulator : [Alacritty](https://github.com/alacritty/alacritty).
	- [exa](https://the.exa.website/).
    - [ripgrep](https://github.com/BurntSushi/ripgrep).
    - [fd](https://github.com/sharkdp/fd).

[Wallpaper](https://www.artstation.com/artwork/e0zz9P)  

This version uses picom-tryone-git (available in the AUR).  
Plans on adding a Neovim config within the next month.  
Plans on switching over to Polybar and remove i3-blocks.  

### TODO : Clean out the dotfiles
